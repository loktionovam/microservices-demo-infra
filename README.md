# microservices-demo-infra

## Local running

* Setup gcp project name and environment slug in the `.env` variables file:

  ```bash
  cp .env.example .env
  ```

* Apply configuration:

  ```bash
  docker-compose up
  ```
