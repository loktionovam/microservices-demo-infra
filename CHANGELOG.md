Changelog
=========


1.0.0 (2020-04-20)
------------------
- Merge branch 'release/1' into 'master' [Aleksandr Loktionov]

  Release/1 #1

  See merge request loktionovam/microservices-demo-infra!16
- GRADUATE-PRJ: Update changelog. [Aleksandr Loktionov]
- Merge branch 'bugfix/1-release-version-number' into 'master'
  [Aleksandr Loktionov]

  Remove unnecessary gitchangelog docker image.

  See merge request loktionovam/microservices-demo-infra!15
- GRADUATE-PRJ: Remove unnecessary gitchangelog docker image. [Aleksandr
  Loktionov]
- Merge branch 'bugfix/1-release-version-number' into 'master'
  [Aleksandr Loktionov]
- GRADUATE-PRJ: Fix version autobump for releases. [Aleksandr Loktionov]
- Merge branch 'bugfix/1-auto-bump-release-branch' into 'master'
  [Aleksandr Loktionov]

  Fix autobump for releases.

  See merge request loktionovam/microservices-demo-infra!13
- GRADUATE-PRJ: Fix autobump for releases. [Aleksandr Loktionov]
- Merge branch 'bugfix/1-auto-bump-release-branch' into 'master'
  [Aleksandr Loktionov]

  Fix release branch regex in bump version #1

  See merge request loktionovam/microservices-demo-infra!12
- GRADUATE-PRJ: Fix release branch regex in bump version. [Aleksandr
  Loktionov]
- Merge branch 'feature/1-deploy-to-prod' into 'master' [Aleksandr
  Loktionov]

  Add deploy to production gitlab ci stage #1

  See merge request loktionovam/microservices-demo-infra!11
- Add deploy to production gitlab ci stage #1. [Aleksandr Loktionov]
- Merge branch 'bugfix/1-deploy-stage-k8s-image-name' into 'master'
  [Aleksandr Loktionov]

  Fix image name for deploy-stage-k8s job #1

  See merge request loktionovam/microservices-demo-infra!10
- Fix image name for deploy-stage-k8s job. [Aleksandr Loktionov]
- Merge branch 'bugfix/1-deploy-stage-k8s' into 'master' [Aleksandr
  Loktionov]

  Deploy on stage from protected tags #1

  See merge request loktionovam/microservices-demo-infra!9
- Deploy on stage from protected tags #1. [Aleksandr Loktionov]
- Merge branch 'feature/1-deploy-to-stage' into 'master' [Aleksandr
  Loktionov]

  Add deploy to stage gitlab ci tasks #1

  See merge request loktionovam/microservices-demo-infra!8
- GRADUATE-PRJ: Add deploy to stage gitlab ci tasks #1. [Aleksandr
  Loktionov]


0.1.0 (2020-04-20)
------------------
- Change image to ubuntu bionic. [Aleksandr Loktionov]
- Remove manual from deploy-stage. [Aleksandr Loktionov]
- Setup deploy-stage as first stage. [Aleksandr Loktionov]
- Add some debug info. [Aleksandr Loktionov]
- Fake commit. [Aleksandr Loktionov]
- GRADUATE-PRJ: Fix deploy-stage-k8s rules. [Aleksandr Loktionov]
- GRADUATE-PRJ: Add deploy-stage to gitlab ci. [Aleksandr Loktionov]
- Merge branch 'feature/autobump-version' into 'master' [Aleksandr
  Loktionov]

  Add autobump repository version.

  See merge request loktionovam/microservices-demo-infra!7
- GRADUATE-PRJ: Add autobump repository version. [Aleksandr Loktionov]
- Merge branch 'feature/add-changelog' into 'master' [Aleksandr
  Loktionov]
- GRADUATE-PRJ: Add changelog generation via gitchangelog. [Aleksandr
  Loktionov]

  Changelog generated via gitchangelog docker image.
- Merge branch 'feature/enable-gke-network-policy' into 'master'
  [Aleksandr Loktionov]

  Enable gke network policy

  See merge request loktionovam/microservices-demo-infra!5
- EX-17-K8S-GITOPS: Setup network policy provider to calico. [Aleksandr
  Loktionov]
- EX-17-K8S-GITOPS: Enable GKE network policy. [Aleksandr Loktionov]
- Merge branch 'feature/disable-gke-istio-addon' into 'master'
  [Aleksandr Loktionov]

  Disable gke istio addon

  See merge request loktionovam/microservices-demo-infra!4
- EX-17-K8S-GITOPS: Disable GKE istio addon. [Aleksandr Loktionov]
- EX-17-K8S-GITOPS: Fix build_terraform_runner. [Aleksandr Loktionov]
- EX-17-K8S-GITOPS: Fix artifact version in .build_docker_template.
  [Aleksandr Loktionov]
- Merge branch 'bugfix/docker-template-build-version' into 'master'
  [Aleksandr Loktionov]

  Fix artifact build version in the gitlab ci docker template builder.

  See merge request loktionovam/microservices-demo-infra!3
- EX-17-K8S-GITOPS: Fix artifact build version in the gitlab ci docker
  template builder. [Aleksandr Loktionov]
- Merge branch 'feature/enable-istio-addon' into 'master' [Aleksandr
  Loktionov]

  Enable istio addon installation to the GKE.

  See merge request loktionovam/microservices-demo-infra!2
- EX-17-K8S-GITOPS: Enable istio addon installation to the GKE.
  [Aleksandr Loktionov]
- Merge branch 'feature/k8s-gitlab-ci' into 'master' [Aleksandr
  Loktionov]

  Add GKE cluster deploy via terraform.

  See merge request loktionovam/microservices-demo-infra!1
- Add GKE cluster deploy via terraform. [Aleksandr Loktionov]
- EX-17-K8S-GITOPS: Initial commit for GKE k8s infrastructure.
  [Aleksandr Loktionov]


