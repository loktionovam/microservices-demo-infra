#!/usr/bin/env python3
"""Simple repository version autobumper.
"""

from git import Repo, GitCommandError
import os
import semver
import re

COMMIT_TITLE = os.getenv('CI_COMMIT_TITLE')
BRANCH = os.getenv('CI_COMMIT_BRANCH')
BUILD_NUMBER = os.getenv('CI_PIPELINE_ID')
PROJECT_DIR = os.getenv('CI_PROJECT_DIR')

print("BRANCH: ", BRANCH)
print("COMMIT_TITLE: ", COMMIT_TITLE)
print("BUILD_NUMBER: ", BUILD_NUMBER)
print("PROJECT_DIR: ", PROJECT_DIR)

tag = None

repo = Repo(PROJECT_DIR)

try:
    tag = repo.git.describe('--tags')
except GitCommandError as error:
    if 'fatal: No names found, cannot describe anything.' in error.stderr:
        print("Can't find any tags.")
        tag = '0.0.0'
    else:
        raise

version = semver.parse_version_info(tag)
print("Current version is: ", version)

is_new_release = False
if re.search("^Merge branch 'release/.*' into 'master'$", COMMIT_TITLE):
    print("Merged branch is release, so bump major.")
    is_new_release = True
    new_version = version.bump_major()
elif re.search("^Merge branch 'feature/.*' into 'master'$", COMMIT_TITLE):
    print("Merged branch is feature, so bump minor.")
    new_version = version.bump_minor()
elif re.search("^Merge branch 'bugfix/.*' into 'master'$", COMMIT_TITLE):
    print("Merged branch is bugfix, so bump patch.")
    new_version = version.bump_patch()
else:
    raise("Wrong title name for autobump.")

if BUILD_NUMBER is None:
    raise("Wrong build number '%s'", BUILD_NUMBER)

if is_new_release:
    new_version = new_version.replace(build=None)
else:
    new_version = new_version.replace(build=BUILD_NUMBER)

print("New version is: ", new_version)

try:
    repo.create_tag(str(new_version))
except GitCommandError as error:
    if 'already exists' in error.stderr:
        print("Something going strange because tag " + str(new_version) + " already exists." )
    else:
        raise
