#!/usr/bin/env bash
# this script file used to develop bump-version docker image.
set -eo pipefail

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# FIXME: split environment variables setup file
# shellcheck source=scripts/set-env.sh
# source "${SCRIPTS_DIR}/set-env.sh"

echo "Push git tags."
cd "${SCRIPTS_DIR}/../"
git config user.email "${GITLAB_USER_EMAIL}"
git config user.name "${GITLAB_USER_NAME}"
git remote add api-origin "https://oauth2:${GITLAB_ACCESS_TOKEN}@gitlab.com/${CI_PROJECT_PATH}"
git push api-origin --tags
