# shellcheck shell=bash

if [ "${ENV_VARS_ARE_SET}" = "true" ]; then
    return
fi

if [ ! "${CI}" ]; then
    # setup local variables here
    readonly IS_LOCAL_EXECUTION="true"
else
    # setup CI related variables here
    readonly IS_LOCAL_EXECUTION="false"
fi

set -u

msg_info() {
    echo "$(date -Iseconds --utc) ${SCRIPT_NAME:-console} $1" >&1
    echo
}

export -f msg_info

die() {
    msg_info "$1"
    exit 1
}

export -f die

DEBUG_MODE="${DEBUG_MODE:-"false"}"
export DEBUG_MODE

if [ "${DEBUG_MODE}" = "true" ]; then
    TF_LOG=TRACE
    export TF_LOG
fi

export IS_LOCAL_EXECUTION
SCRIPT_NAME=$(basename "$0")

export SCRIPT_NAME

TERRAFORM_DIR="${SCRIPTS_DIR}/../kubernetes/terraform"
export TERRAFORM_DIR

BOOTSTRAP_DIR="${SCRIPTS_DIR}/../kubernetes/bootstrap"
export BOOTSTRAP_DIR

case "${CI_ENVIRONMENT_SLUG}" in
    dev|stage|prod)
        msg_info "Running in ${CI_ENVIRONMENT_SLUG} environment."
        ;;
    *)
        msg_info "Selected a wrong environment CI_ENVIRONMENT_SLUG=${CI_ENVIRONMENT_SLUG}"
        die "Valid environment is dev|stage|prod"
        ;;
esac

case "${CI_TERRAFORM_ACTION}" in
    apply|destroy|plan)
        msg_info "Selected terraform action is: ${CI_TERRAFORM_ACTION}"
        ;;
    *)
        die "Selected wrong action ${CI_TERRAFORM_ACTION}. Valid is apply|destroy|plan"
        ;;
esac

GOOGLE_CLOUD_KEYFILE_JSON="${CI_GOOGLE_CREDENTIALS_FILE:-./secrets/gcp-credentials.json}"
export GOOGLE_CLOUD_KEYFILE_JSON

# GOOGLE_CREDENTIALS_FILE="${GOOGLE_CLOUD_KEYFILE_JSON}"
# export GOOGLE_CREDENTIALS_FILE

# GOOGLE_CREDENTIALS=$(cat "${GOOGLE_CREDENTIALS_FILE}")
# export GOOGLE_CREDENTIALS

# CLOUDSDK_CONFIG="${CI_CLOUDSDK_CONFIG:-./secrets}"
# export CLOUDSDK_CONFIG

GOOGLE_CREDENTIALS=$(cat "${GOOGLE_CLOUD_KEYFILE_JSON}")

export GOOGLE_CREDENTIALS

# GOOGLE_APPLICATION_CREDENTIALS="${CI_GOOGLE_CREDENTIALS_FILE}"
# export GOOGLE_APPLICATION_CREDENTIALS

CLOUDSDK_AUTH_CREDENTIAL_FILE_OVERRIDE="${CI_GOOGLE_CREDENTIALS_FILE}"
export CLOUDSDK_AUTH_CREDENTIAL_FILE_OVERRIDE

export CI_GCP_PROJECT

USER_ID="$(id -u)"
GROUP_ID="$(id -g)"

export USER_ID
export GROUP_ID

print_variables() {
    # Explicitly allow to output undefined variables
    set +u
    for VAR in  CI \
                IS_LOCAL_EXECUTION \
                SCRIPT_NAME \
                TERRAFORM_DIR \
                CI_ENVIRONMENT_SLUG \
                CI_TERRAFORM_ACTION \
                GOOGLE_CLOUD_KEYFILE_JSON \
                DEBUG_MODE \
                TF_LOG \
                USER_ID \
                GROUP_ID
    do
        echo "${VAR}: ${!VAR}"
    done
    set -u
}

export -f print_variables

print_variables

readonly ENV_VARS_ARE_SET="true"
export ENV_VARS_ARE_SET
