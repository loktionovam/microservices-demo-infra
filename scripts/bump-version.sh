#!/usr/bin/env bash
# this script file used to develop bump-version docker image.
set -eo pipefail

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Build autobump version container."
docker-compose -f "${SCRIPTS_DIR}/../docker-compose-bump-version.yml" build

msg_info "Start autobump version container."
docker-compose -f "${SCRIPTS_DIR}/../docker-compose-bump-version.yml" up
