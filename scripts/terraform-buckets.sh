#!/usr/bin/env bash
set -eo pipefail
# This script used to deploy GKE k8s managed cluster via terraform

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Init terrafrom bucket configuration."
cd "${TERRAFORM_DIR}/"
terraform init

if ! terraform apply -var project="${CI_GCP_PROJECT}" -var environment="${CI_ENVIRONMENT_SLUG}" -auto-approve; then
    msg_info "Can't apply new configuration for bucket, so try to import it."
    terraform import \
        -var project="${CI_GCP_PROJECT}" \
        google_storage_bucket.storage-bucket \
        "kubernetes-tf-state-bucket-${CI_ENVIRONMENT_SLUG}"
else
    msg_info "Successfully created GCP buckets."
fi
