#!/usr/bin/env bash
set -eo pipefail
# This script used to deploy GKE k8s managed cluster via terraform

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

"${SCRIPTS_DIR}/terraform-buckets.sh"
"${SCRIPTS_DIR}/terraform-setup-configuration.sh"
"${SCRIPTS_DIR}/terraform-gke-cluster.sh"
"${SCRIPTS_DIR}/bootstrap-cluster.sh"
