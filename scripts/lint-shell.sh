#!/bin/sh

set -eu

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

find "${SCRIPTS_DIR}" -name "*.sh" -exec shellcheck {} +

echo "Shellcheck completed, no errors found."
