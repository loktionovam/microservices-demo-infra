#!/usr/bin/env bash
set -eo pipefail
# This script used to deploy GKE k8s managed cluster via terraform

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Bootstrap GKE managed cluster."
cd "${BOOTSTRAP_DIR}/"

msg_info "Get cluster credentials."
gcloud container clusters get-credentials "${CI_GKE_CLUSTER_NAME}" --zone "${CI_GCP_ZONE}" --project "${CI_GCP_PROJECT}"

kubectl apply -f flux-namespace.yml

# FIXME: setup google service account which can create clusterrole bindings
helm repo add fluxcd https://charts.fluxcd.io
helm upgrade --install flux fluxcd/flux -f flux.values.yaml --namespace flux
helm upgrade --install helm-operator fluxcd/helm-operator -f helm-operator.values.yaml --namespace flux
