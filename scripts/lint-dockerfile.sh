#!/bin/sh

set -eu
# set -x
find "./" -name "Dockerfile*" -exec hadolint {} +

echo "Hadolint completed, no errors found."
