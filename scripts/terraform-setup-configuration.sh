#!/usr/bin/env bash
set -eo pipefail
# This script used to deploy GKE k8s managed cluster via terraform

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Copy ${CI_ENVIRONMENT_SLUG}/terraform.tfvars to ${TERRAFORM_DIR}/gke/"
cp -v "${TERRAFORM_DIR}/environments/${CI_ENVIRONMENT_SLUG}/terraform.tfvars.example" "${TERRAFORM_DIR}/gke/terraform.tfvars"

msg_info "Copy ${CI_ENVIRONMENT_SLUG}/backend.tf to ${TERRAFORM_DIR}/gke/"
cp -v "${TERRAFORM_DIR}/environments/${CI_ENVIRONMENT_SLUG}/backend.tf" "${TERRAFORM_DIR}/gke/"
