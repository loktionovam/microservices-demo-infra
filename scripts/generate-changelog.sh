#!/usr/bin/env bash

set -eo pipefail

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Build gitchangelog container."
docker-compose -f "${SCRIPTS_DIR}/../docker-compose-changelog.yml" build

msg_info "Start gitchangelog container."
docker-compose -f "${SCRIPTS_DIR}/../docker-compose-changelog.yml" up
