#!/usr/bin/env bash
set -eo pipefail
# This script used to deploy GKE k8s managed cluster via terraform

SCRIPTS_DIR=$(dirname "$(readlink --canonicalize-existing "$0")")

# shellcheck source=scripts/set-env.sh
source "${SCRIPTS_DIR}/set-env.sh"

msg_info "Create ${CI_ENVIRONMENT_SLUG} GKE managed cluster."
cd "${TERRAFORM_DIR}/gke/"
terraform init

if [ "${DEBUG_MODE}" = "true" ] || [ "${CI_TERRAFORM_ACTION}" = "plan" ]; then
    msg_info "DEBUG_MODE is 'true' so run 'terraform plan' first."
    terraform plan -var project="${CI_GCP_PROJECT}"
fi

if [ "${CI_TERRAFORM_ACTION}" = "apply" ] || [ "${CI_TERRAFORM_ACTION}" = "destroy" ]; then
    terraform "${CI_TERRAFORM_ACTION}" -var project="${CI_GCP_PROJECT}" -auto-approve
fi
