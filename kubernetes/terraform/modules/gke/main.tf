provider "google-beta" {
  version = "3.6.0"
  project = var.project
  region  = var.region
}

resource "google_container_cluster" "primary" {
  provider           = google-beta
  name               = var.cluster_name
  location           = var.location
  min_master_version = var.min_master_version
  subnetwork         = var.subnetwork
  logging_service    = var.logging_service
  monitoring_service = var.monitoring_service
  node_pool {
    name       = "default-pool"
    node_count = var.defaultpool_nodes_count

    node_config {
      machine_type = var.defaultpool_machine_type
      disk_size_gb = var.defaultpool_machine_size
    }
  }
  node_pool {
    name       = "infra-pool"
    node_count = var.infrapool_nodes_count

    node_config {
      machine_type = var.infrapool_machine_type
      disk_size_gb = var.infrapool_machine_size
    }
  }
  addons_config {
    istio_config {
      disabled = var.istio_disabled
      auth     = "AUTH_MUTUAL_TLS"
    }
    network_policy_config {
      disabled = !var.network_policy_enabled
    }
  }
  network_policy {
    enabled = var.network_policy_enabled
    provider = "CALICO"
  }
}

resource "google_compute_firewall" "firewall_kubernetes" {
  name    = "allow-kubernetes"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["30000-32767"]
  }

  source_ranges = ["0.0.0.0/0"]
}
