variable "project" {
  description = "Project ID"
}

variable "region" {
  description = "Region"
  default     = "europe-west1"
}

variable "environment" {
  description = "Environment, e.g. 'prod', 'staging', 'dev"
  default     = "dev"
}
