// Function calls not allowed for backend,
// so create backend config per environment
terraform {
  backend "gcs" {
    bucket = "kubernetes-tf-state-bucket-dev"
    prefix = "gke/terraform/state"
  }
}
