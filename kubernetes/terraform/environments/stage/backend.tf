// Function calls not allowed for backend,
// so create backend config per environment
terraform {
  backend "gcs" {
    bucket = "kubernetes-tf-state-bucket-stage"
    prefix = "gke/terraform/state"
  }
}
